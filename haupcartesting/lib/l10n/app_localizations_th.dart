import 'app_localizations.dart';

/// The translations for Thai (`th`).
class AppLocalizationsTh extends AppLocalizations {
  AppLocalizationsTh([String locale = 'th']) : super(locale);

  @override
  String get home => 'หน้าหลัก';

  @override
  String get categories => 'หมวดหมู่';

  @override
  String get language => 'ภาษา';

  @override
  String get price => 'ราคา';

  @override
  String get discount => 'ส่วนลด';

  @override
  String get rating => 'คะแนน';
}
