import 'app_localizations.dart';

/// The translations for English (`en`).
class AppLocalizationsEn extends AppLocalizations {
  AppLocalizationsEn([String locale = 'en']) : super(locale);

  @override
  String get home => 'Home';

  @override
  String get categories => 'categories';

  @override
  String get language => 'Language';

  @override
  String get price => 'Price';

  @override
  String get discount => 'Discount';

  @override
  String get rating => 'Rating';
}
