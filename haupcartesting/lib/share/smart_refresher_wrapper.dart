import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:haupcartesting/config/theme.dart';

import 'package:pull_to_refresh/pull_to_refresh.dart';

class SmartRefresherWrapper extends StatefulWidget {
  final Widget child;
  final Future<void> Function() onRefresh;
  const SmartRefresherWrapper(
      {required this.child, required this.onRefresh, Key? key})
      : super(key: key);

  @override
  State<SmartRefresherWrapper> createState() => _SmartRefresherWrapperState();
}

class _SmartRefresherWrapperState extends State<SmartRefresherWrapper>
    with TickerProviderStateMixin {
  late RefreshController _refreshController;
  void enterRefresh() {
    _refreshController.requestLoading();
  }

  @override
  void initState() {
    _refreshController = RefreshController(initialRefresh: false);
    super.initState();
  }

  @override
  void dispose() {
    _refreshController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SmartRefresher(
      onRefresh: () async {
        await widget.onRefresh();
        _refreshController.refreshCompleted();
      },
      enablePullUp: false,
      enablePullDown: true,
      controller: _refreshController,
      header: const ClassicHeader(
        releaseText: 'Release to refresh',
        refreshingText: 'Refreshing',
        completeText: 'Refresh completed',
        idleText: 'Pull down refresh',
        textStyle: TextStyle(color: surfaceColor),
        failedIcon: Icon(Icons.error, color: surfaceColor),
        completeIcon: Icon(Icons.done, color: surfaceColor),
        idleIcon: Icon(Icons.arrow_downward, color: surfaceColor),
        releaseIcon: Icon(Icons.refresh, color: surfaceColor),
        refreshingIcon:
            CupertinoActivityIndicator(radius: 8, color: surfaceColor),
      ),
      child: widget.child,
    );
  }
}
