import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:haupcartesting/config/language.dart';
import 'package:haupcartesting/config/theme.dart';
import 'package:haupcartesting/l10n/app_localizations.dart';
import 'package:haupcartesting/logic/cubit/language_cubit.dart';

class PageWrapper extends StatelessWidget {
  final Widget child;
  final Widget? bottomBar;
  final String? title;
  const PageWrapper({required this.child, this.title, this.bottomBar, Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final l10n = AppLocalizations.of(context);
    return BlocBuilder<LanguageCubit, LanguageState>(
      builder: (context, state) {
        return GestureDetector(
          onTap: () => FocusManager.instance.primaryFocus?.unfocus(),
          child: Scaffold(
              bottomNavigationBar: bottomBar,
              resizeToAvoidBottomInset: false,
              appBar: AppBar(
                  elevation: 0,
                  centerTitle: true,
                  title: Text('$title',
                      style: Theme.of(context).textTheme.headlineMedium!)),
              endDrawer: Drawer(
                child: ListView(
                  padding: EdgeInsets.only(top: 50.h),
                  children: [
                    SizedBox(
                      height: 120.0.h,
                      child: DrawerHeader(
                        decoration:
                            const BoxDecoration(color: primaryDarkColor),
                        padding: EdgeInsets.only(bottom: 0.h),
                        child: Container(
                          margin: EdgeInsets.only(left: 20.w),
                          child: Text(l10n.language,
                              style: Theme.of(context).textTheme.displayLarge!),
                        ),
                      ),
                    ),
                    ListTile(
                      leading: ClipOval(
                        child: Image(
                          image: const AssetImage(
                              "assets/images/thai_language.png"),
                          height: 32.0.h,
                          width: 32.0.w,
                        ),
                      ),
                      title: Text(Language.thailand.text,
                          style: Theme.of(context).textTheme.bodyMedium!),
                      trailing: state.selectedLanguage.value.countryCode == "TH"
                          ? const Icon(
                              Icons.check_circle_rounded,
                              color: primaryDarkColor,
                            )
                          : null,
                      onTap: () {
                        context.read<LanguageCubit>().onChangeLanguage(
                            Language.thailand.value.languageCode);
                        Future.delayed(const Duration(milliseconds: 300))
                            .then((value) => Navigator.of(context).pop());
                      },
                    ),
                    ListTile(
                      leading: ClipOval(
                        child: Image(
                          image: const AssetImage(
                              "assets/images/english_language.png"),
                          height: 32.0.h,
                          width: 32.0.w,
                        ),
                      ),
                      title: Text(Language.english.text,
                          style: Theme.of(context).textTheme.bodyMedium!),
                      trailing: state.selectedLanguage.value.countryCode == "US"
                          ? const Icon(
                              Icons.check_circle_rounded,
                              color: primaryDarkColor,
                            )
                          : null,
                      onTap: () {
                        context.read<LanguageCubit>().onChangeLanguage(
                            Language.english.value.languageCode);
                        Future.delayed(const Duration(milliseconds: 300))
                            .then((value) => Navigator.of(context).pop());
                      },
                    ),
                  ],
                ),
              ),
              body: SafeArea(
                child: Container(
                    height: 1.sh,
                    width: 1.sw,
                    decoration: const BoxDecoration(
                        gradient: LinearGradient(
                      begin: Alignment.topCenter,
                      end: Alignment.bottomCenter,
                      colors: [
                        Color(0xff00347b),
                        Color(0xff0046a0),
                        Color(0xff4b8adc),
                      ],
                    )),
                    child: child),
              )),
        );
      },
    );
  }
}
