import 'package:flutter_bloc/flutter_bloc.dart';

import 'dart:developer' as developer;

class AppBlocObserver extends BlocObserver {
  final logName = 'BLoC';

  @override
  void onChange(BlocBase bloc, Change change) {
    developer.log('[onChange] ${'-' * 50}', name: logName);
    developer.log('CurrentState:\t${change.currentState.toString()}',
        name: logName);
    developer.log('NextState:\t${change.nextState.toString()}', name: logName);
    // developer.log('-' * 61, name: logName);

    super.onChange(bloc, change);
  }

  @override
  // ignore: unnecessary_overrides
  void onClose(BlocBase bloc) {
    // implement onClose
    developer.log('$bloc closed.', name: logName);

    super.onClose(bloc);
  }

  @override
  void onCreate(BlocBase bloc) {
    developer.log('[onCreate] $bloc created.', name: logName);

    super.onCreate(bloc);
  }

  @override
  // ignore: unnecessary_overrides
  void onError(BlocBase bloc, Object error, StackTrace stackTrace) {
    // implement onError
    super.onError(bloc, error, stackTrace);
  }

  @override
  // ignore: unnecessary_overrides
  void onEvent(Bloc bloc, Object? event) {
    // implement onEvent
    super.onEvent(bloc, event);
  }

  @override
  // ignore: unnecessary_overrides
  void onTransition(Bloc bloc, Transition transition) {
    // implement onTransition
    super.onTransition(bloc, transition);
  }
}
