import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:http/http.dart' as http;

class CategoryRepository {
  Future<List<String>?> fetchCategoryInfo() async {
    try {
      var url = Uri.parse('https://dummyjson.com/products/categories');
      final response = await http.get(url).timeout(const Duration(seconds: 20),
          onTimeout: () {
        throw TimeoutException(
            'The connection has timed out, Please try again!');
      });

      if (response.statusCode == HttpStatus.ok) {
        final jsonData = jsonDecode(response.body);
        final List<String> result = jsonData.cast<String>();
        return result;
      } else {
        throw Exception('Failed to load users');
      }
    } on SocketException {
      print("You are not connected to internet");
      return null;
    }
  }
}
