import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:haupcartesting/logic/model/product_model.dart';
import 'package:http/http.dart' as http;

class ProductRepository {
  Future<List<ProductModel>?> fetchProductInfo(String productName) async {
    try {
      var url =
          Uri.parse('https://dummyjson.com/products/category/$productName');
      final response = await http.get(url).timeout(const Duration(seconds: 20),
          onTimeout: () {
        throw TimeoutException(
            'The connection has timed out, Please try again!');
      });

      if (response.statusCode == HttpStatus.ok) {
        final jsonData = jsonDecode(response.body);
        final dynamicList = jsonData['products'] as List<dynamic>;
        final result =
            dynamicList.map((json) => ProductModel.fromJson(json)).toList();

        return result;
      } else {
        throw Exception('Failed to load users');
      }
    } on SocketException {
      print("You are not connected to internet");
      return null;
    }
  }
}
