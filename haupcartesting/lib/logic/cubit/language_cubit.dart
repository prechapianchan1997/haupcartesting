import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:haupcartesting/config/language.dart';
import 'package:shared_preferences/shared_preferences.dart';

part 'language_state.dart';

class LanguageCubit extends Cubit<LanguageState> {
  LanguageCubit() : super(LanguageInitial());

  Future<void> onChangeStartLang() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String? langCode = prefs.getString('lang');
    emit(LanguageFetched(
        selectedLanguage:
            langCode == 'en' ? Language.english : Language.thailand));
  }

  Future<void> onChangeLanguage(String langCode) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString('lang', langCode);

    emit(LanguageFetched(
        selectedLanguage:
            langCode == 'en' ? Language.english : Language.thailand));
  }
}
