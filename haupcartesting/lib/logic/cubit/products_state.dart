part of 'products_cubit.dart';

abstract class ProductsState extends Equatable {
  final List<ProductModel>? products;

  const ProductsState({this.products});

  @override
  List<Object> get props => [];
}

class ProductsInitial extends ProductsState {}

class ProductseLoading extends ProductsState {}

class ProductsFetched extends ProductsState {
  const ProductsFetched({required super.products});

  ProductsFetched copyWith({required List<ProductModel> products}) {
    return ProductsFetched(products: products);
  }
}

class ProductsError extends ProductsState {}
