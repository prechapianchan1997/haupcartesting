part of 'language_cubit.dart';

abstract class LanguageState extends Equatable {
  final Language selectedLanguage;

  const LanguageState({
    Language? selectedLanguage,
  }) : selectedLanguage = selectedLanguage ?? Language.english;

  @override
  List<Object> get props => [selectedLanguage];
}

class LanguageInitial extends LanguageState {}

class LanguageeLoading extends LanguageState {}

class LanguageFetched extends LanguageState {
  const LanguageFetched({required super.selectedLanguage});

  LanguageFetched copyWith({required Language selectedLanguage}) {
    return LanguageFetched(selectedLanguage: selectedLanguage);
  }
}

class LanguageError extends LanguageState {}
