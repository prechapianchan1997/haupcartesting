part of 'category_cubit.dart';

abstract class CategoryState extends Equatable {
  final List<String>? category;

  const CategoryState({this.category});

  @override
  List<dynamic> get props => [category];
}

class CategoryInitial extends CategoryState {}

class CategoryeLoading extends CategoryState {}

class CategoryFetched extends CategoryState {
  const CategoryFetched({required super.category});

  CategoryFetched copyWith({required List<String> category}) {
    return CategoryFetched(category: category);
  }
}

class CategoryError extends CategoryState {}
