import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:haupcartesting/logic/model/product_model.dart';
import 'package:haupcartesting/logic/repo/product_repository.dart';

part 'products_state.dart';

class ProductsCubit extends Cubit<ProductsState> {
  ProductsCubit() : super(ProductsInitial());

  final ProductRepository _productRepository = ProductRepository();

  Future<void> fetchProductsData(String productName) async {
    await _productRepository
        .fetchProductInfo(productName)
        .then((products) => emit(ProductsFetched(products: products)))
        .onError((error, stackTrace) => emit(ProductsError()));
  }
}
