import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:haupcartesting/logic/repo/category_repository.dart';

part 'category_state.dart';

class CategoryCubit extends Cubit<CategoryState> {
  CategoryCubit() : super(CategoryInitial());

  final CategoryRepository _categoryRepository = CategoryRepository();

  Future<void> fetchCategoryData() async {
    await _categoryRepository
        .fetchCategoryInfo()
        .then((category) => emit(CategoryFetched(category: category)))
        .onError((error, stackTrace) => emit(CategoryError()));
  }
}
