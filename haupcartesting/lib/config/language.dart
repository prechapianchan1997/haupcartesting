import 'dart:ui';

enum Language {
  english(
    Locale('en', 'US'),
    'English',
  ),
  thailand(
    Locale('th', 'TH'),
    'ไทย',
  );

  /// Add another languages support here
  const Language(this.value, this.text);

  final Locale value;
  final String text; // Optional: this properties used for ListTile details
}
