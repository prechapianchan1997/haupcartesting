import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

const primaryColor = Color(0xff264e96);
const primaryDarkColor = Color(0xff102955);
const surfaceColor = Color(0xfff2f6f9);
const tertiaryColor = Color(0xff1a9abe);
const errorColor = Color(0xFFE55350);
const commonWhite = Colors.white;
const commonBlack = Colors.black;
const commonGrey = Colors.grey;

final appTheme = ThemeData(
    fontFamily: 'IBMPlexSansThai',
    backgroundColor: commonWhite,
    appBarTheme: const AppBarTheme(
        titleTextStyle: TextStyle(color: primaryColor),
        iconTheme: IconThemeData(color: primaryColor),
        color: surfaceColor,
        foregroundColor: surfaceColor),
    scaffoldBackgroundColor: commonWhite,
    textTheme: TextTheme(
      bodyLarge: TextStyle(
          fontSize: 20.sp, color: commonWhite, fontWeight: FontWeight.normal),
      bodyMedium: TextStyle(
          color: primaryDarkColor,
          fontSize: 18.sp,
          fontWeight: FontWeight.normal),
      bodySmall: TextStyle(
          color: primaryDarkColor,
          fontSize: 16.sp,
          fontWeight: FontWeight.normal),
      displayLarge: TextStyle(
          color: commonWhite, fontSize: 26.sp, fontWeight: FontWeight.bold),
      displayMedium: TextStyle(
          color: commonWhite, fontSize: 24.sp, fontWeight: FontWeight.w700),
      displaySmall: TextStyle(
          color: primaryDarkColor,
          fontSize: 22.sp,
          fontWeight: FontWeight.w700),
      headlineLarge: TextStyle(
          color: primaryDarkColor,
          fontSize: 30.sp,
          fontWeight: FontWeight.w700),
      headlineMedium: TextStyle(
          color: primaryDarkColor,
          fontSize: 26.sp,
          fontWeight: FontWeight.w700),
      headlineSmall: TextStyle(
          color: primaryDarkColor,
          fontSize: 22.sp,
          fontWeight: FontWeight.w700),
    ),
    primaryColor: primaryColor,
    disabledColor: commonGrey.shade400,
    errorColor: errorColor,
    colorScheme: const ColorScheme(
        background: commonWhite,
        onSurface: primaryColor, //Toggle bg color default primaryColor
        surface: surfaceColor,
        primary: primaryColor,
        onPrimary: commonWhite, //Static bg color - white
        secondary: commonWhite,
        onSecondary: commonWhite,
        tertiary: tertiaryColor,
        onTertiary: tertiaryColor,
        onBackground: primaryDarkColor,
        error: errorColor,
        onError: errorColor,
        brightness: Brightness.light));

final appDarkTheme = ThemeData(
    fontFamily: 'IBMPlexSansThai',
    backgroundColor: commonWhite,
    appBarTheme: const AppBarTheme(
        titleTextStyle: TextStyle(color: primaryColor),
        iconTheme: IconThemeData(color: primaryColor),
        color: surfaceColor,
        foregroundColor: surfaceColor),
    scaffoldBackgroundColor: commonWhite,
    textTheme: TextTheme(
      bodyLarge: TextStyle(
          fontSize: 20.sp, color: commonWhite, fontWeight: FontWeight.normal),
      bodyMedium: TextStyle(
          color: primaryDarkColor,
          fontSize: 18.sp,
          fontWeight: FontWeight.normal),
      bodySmall: TextStyle(
          color: primaryDarkColor,
          fontSize: 16.sp,
          fontWeight: FontWeight.normal),
      displayLarge: TextStyle(
          color: commonWhite, fontSize: 26.sp, fontWeight: FontWeight.bold),
      displayMedium: TextStyle(
          color: commonWhite, fontSize: 24.sp, fontWeight: FontWeight.w700),
      displaySmall: TextStyle(
          color: primaryDarkColor,
          fontSize: 22.sp,
          fontWeight: FontWeight.w700),
      headlineLarge: TextStyle(
          color: primaryDarkColor,
          fontSize: 30.sp,
          fontWeight: FontWeight.w700),
      headlineMedium: TextStyle(
          color: primaryDarkColor,
          fontSize: 26.sp,
          fontWeight: FontWeight.w700),
      headlineSmall: TextStyle(
          color: primaryDarkColor,
          fontSize: 22.sp,
          fontWeight: FontWeight.w700),
    ),
    primaryColor: primaryColor,
    disabledColor: commonGrey.shade400,
    errorColor: errorColor,
    colorScheme: const ColorScheme(
        background: commonWhite,
        onSurface: primaryColor, //Toggle bg color default primaryColor
        surface: surfaceColor,
        primary: primaryColor,
        onPrimary: commonWhite, //Static bg color - white
        secondary: commonWhite,
        onSecondary: commonWhite,
        tertiary: tertiaryColor,
        onTertiary: tertiaryColor,
        onBackground: primaryDarkColor,
        error: errorColor,
        onError: errorColor,
        brightness: Brightness.dark));
