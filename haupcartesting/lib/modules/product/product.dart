import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:haupcartesting/config/theme.dart';
import 'package:haupcartesting/l10n/app_localizations.dart';
import 'package:haupcartesting/logic/cubit/products_cubit.dart';
import 'package:haupcartesting/share/page_wrapper.dart';

class ProductPage extends StatefulWidget {
  const ProductPage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<ProductPage> createState() => _ProductPageState();
}

class _ProductPageState extends State<ProductPage> {
  @override
  Widget build(BuildContext context) {
    final l10n = AppLocalizations.of(context);
    return PageWrapper(
      title: widget.title,
      child: MultiBlocProvider(
        providers: [
          BlocProvider<ProductsCubit>(
              create: (context) =>
                  ProductsCubit()..fetchProductsData(widget.title)),
        ],
        child: BlocBuilder<ProductsCubit, ProductsState>(
            builder: (context, state) {
          return state.products == null
              ? const Center(
                  child: CircularProgressIndicator(
                    color: Color(0xffFFFFFF),
                  ),
                )
              : Container(
                  margin: EdgeInsets.only(top: 20.h),
                  child: ListView.builder(
                    itemCount: state.products!.length,
                    shrinkWrap: true,
                    scrollDirection: Axis.vertical,
                    itemBuilder: (BuildContext context, int index) {
                      return Container(
                        margin: EdgeInsets.symmetric(
                            horizontal: 10.w, vertical: 10.h),
                        height: 320.h,
                        child: Card(
                          color: surfaceColor,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(12.0),
                          ),
                          elevation: 0,
                          child: Container(
                            width: MediaQuery.of(context).size.width,
                            margin: EdgeInsets.symmetric(
                                vertical: 10.h, horizontal: 15.w),
                            child: Column(
                              children: [
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Container(
                                      margin: const EdgeInsets.all(10),
                                      width: 150.w,
                                      height: 120.h,
                                      child: CachedNetworkImage(
                                        imageUrl: state
                                            .products![index].thumbnail!
                                            .toString(),
                                        imageBuilder:
                                            (context, imageProvider) =>
                                                Container(
                                          decoration: BoxDecoration(
                                            // shape: BoxShape.circle,
                                            image: DecorationImage(
                                              image: imageProvider,
                                              fit: BoxFit.cover,
                                            ),
                                          ),
                                        ),
                                        errorWidget: (context, url, error) =>
                                            const Icon(Icons.error),
                                      ),
                                    ),
                                    Container(
                                      margin: const EdgeInsets.all(10),
                                      width: MediaQuery.of(context).size.width -
                                          250.w,
                                      child: Column(
                                        children: [
                                          Container(
                                            alignment: Alignment.centerLeft,
                                            child: Text(
                                                '${state.products![index].title}',
                                                overflow: TextOverflow.ellipsis,
                                                style: Theme.of(context)
                                                    .textTheme
                                                    .headlineSmall!),
                                          ),
                                          Container(
                                            alignment: Alignment.centerLeft,
                                            child: RichText(
                                              text: TextSpan(
                                                children: <TextSpan>[
                                                  TextSpan(
                                                      text: l10n.price,
                                                      style: Theme.of(context)
                                                          .textTheme
                                                          .bodyMedium!),
                                                  TextSpan(
                                                      text:
                                                          '  ${state.products![index].price}\$',
                                                      style: TextStyle(
                                                          fontSize: 20.sp,
                                                          color: Colors.red,
                                                          fontWeight:
                                                              FontWeight.w600)),
                                                ],
                                              ),
                                            ),
                                          ),
                                          Container(
                                            margin: EdgeInsets.only(top: 0.h),
                                            alignment: Alignment.centerLeft,
                                            child: RichText(
                                              text: TextSpan(
                                                children: <TextSpan>[
                                                  TextSpan(
                                                      text: l10n.discount,
                                                      style: Theme.of(context)
                                                          .textTheme
                                                          .bodyMedium!),
                                                  TextSpan(
                                                      text:
                                                          '  ${state.products![index].discountPercentage}%',
                                                      style: TextStyle(
                                                          fontSize: 20.sp,
                                                          color: Colors.red,
                                                          fontWeight:
                                                              FontWeight.w600)),
                                                ],
                                              ),
                                            ),
                                          ),
                                          Container(
                                            margin: EdgeInsets.only(top: 0.h),
                                            alignment: Alignment.centerLeft,
                                            child: RichText(
                                              text: TextSpan(
                                                children: <TextSpan>[
                                                  TextSpan(
                                                      text: l10n.rating,
                                                      style: Theme.of(context)
                                                          .textTheme
                                                          .bodyMedium!),
                                                  TextSpan(
                                                      text:
                                                          '  ${state.products![index].rating}',
                                                      style: Theme.of(context)
                                                          .textTheme
                                                          .bodySmall!),
                                                ],
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                                Container(
                                  margin: EdgeInsets.only(top: 5.h),
                                  alignment: Alignment.centerLeft,
                                  child: Text(
                                      '${state.products![index].description}',
                                      overflow: TextOverflow.ellipsis,
                                      maxLines: 5,
                                      style: Theme.of(context)
                                          .textTheme
                                          .bodySmall!),
                                ),
                              ],
                            ),
                          ),
                        ),
                      );
                    },
                  ),
                );
        }),
      ),
    );
  }
}
