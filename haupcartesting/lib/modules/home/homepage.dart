import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:haupcartesting/config/theme.dart';
import 'package:haupcartesting/l10n/app_localizations.dart';
import 'package:haupcartesting/logic/cubit/category_cubit.dart';
import 'package:haupcartesting/modules/product/product.dart';
import 'package:haupcartesting/share/page_wrapper.dart';
import 'package:haupcartesting/share/smart_refresher_wrapper.dart';

class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final l10n = AppLocalizations.of(context);
    return PageWrapper(
      title: l10n.home,
      child: BlocBuilder<CategoryCubit, CategoryState>(
        builder: (context, state) {
          return state.category == null
              ? const Center(
                  child: CircularProgressIndicator(
                    color: Color(0xffFFFFFF),
                  ),
                )
              : SmartRefresherWrapper(
                  onRefresh: () async {
                    await context.read<CategoryCubit>().fetchCategoryData();
                  },
                  child: Column(
                    children: [
                      Container(
                        margin: EdgeInsets.symmetric(
                            horizontal: 20.w, vertical: 10.h),
                        child: Text(l10n.categories,
                            style: Theme.of(context).textTheme.displayLarge!),
                      ),
                      Container(
                        margin: EdgeInsets.only(
                            left: 20.w, right: 20.w, bottom: 10.h),
                        child: Divider(
                          color: const Color(0xffCDD5FF),
                          thickness: 1.h,
                          height: 1.h,
                        ),
                      ),
                      Expanded(
                        child: ListView.builder(
                          itemCount: state.category!.length,
                          shrinkWrap: true,
                          scrollDirection: Axis.vertical,
                          itemBuilder: (BuildContext context, int index) {
                            return GestureDetector(
                              onTap: () => {
                                Navigator.of(context).push(MaterialPageRoute(
                                    builder: (_) => ProductPage(
                                          title: state.category![index],
                                        ))),
                              },
                              child: Container(
                                margin: EdgeInsets.symmetric(
                                    horizontal: 10.w, vertical: 5.h),
                                height: 100.h,
                                child: Card(
                                  color: surfaceColor,
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(12.0),
                                  ),
                                  elevation: 0,
                                  child: Container(
                                    padding: const EdgeInsets.all(10),
                                    alignment: Alignment.centerLeft,
                                    child: Text(state.category![index],
                                        style: Theme.of(context)
                                            .textTheme
                                            .headlineSmall!),
                                  ),
                                ),
                              ),
                            );
                          },
                        ),
                      ),
                    ],
                  ),
                );
        },
      ),
    );
  }
}
