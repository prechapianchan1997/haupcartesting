import 'package:flutter/cupertino.dart';
import 'package:haupcartesting/modules/home/homepage.dart';
import 'package:haupcartesting/modules/product/product.dart';

class AppRoute {
  static const home = 'home';
  static const product = 'product';

  final _route = <String, WidgetBuilder>{
    home: (context) => const HomePage(),
    product: (context) => const ProductPage(
          title: "ProductPage",
        ),
  };

  get getAll => _route;
}
