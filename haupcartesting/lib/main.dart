import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:haupcartesting/config/theme.dart';
import 'package:haupcartesting/l10n/app_localizations.dart';
import 'package:haupcartesting/logic/cubit/category_cubit.dart';
import 'package:haupcartesting/logic/cubit/language_cubit.dart';
import 'package:haupcartesting/logic/repo/category_repository.dart';
import 'package:haupcartesting/logic/repo/product_repository.dart';
import 'package:haupcartesting/logic/utils/app_bloc_observer.dart';
import 'package:haupcartesting/modules/home/homepage.dart';
import 'package:haupcartesting/routes/app_route.dart';

Future<void> main() async {
  Bloc.observer = AppBlocObserver();
  runApp(
    const HaupcarTestingApp(),
  );
}

class HaupcarTestingApp extends StatefulWidget {
  const HaupcarTestingApp({Key? key}) : super(key: key);

  @override
  HaupcarTestingAppState createState() => HaupcarTestingAppState();
}

class HaupcarTestingAppState extends State<HaupcarTestingApp> {
  @override
  Widget build(BuildContext context) {
    return MultiRepositoryProvider(
      providers: [
        RepositoryProvider(create: (_) => CategoryRepository()),
        RepositoryProvider(create: (_) => ProductRepository()),
      ],
      child: MultiBlocProvider(
        providers: [
          BlocProvider<CategoryCubit>(
              create: (context) => CategoryCubit()..fetchCategoryData()),
          BlocProvider<LanguageCubit>(
              create: (context) => LanguageCubit()..onChangeStartLang()),
        ],
        child: ScreenUtilInit(
          designSize: const Size(428, 926),
          minTextAdapt: true,
          splitScreenMode: true,
          builder: (context, child) {
            return BlocBuilder<LanguageCubit, LanguageState>(
                builder: (context, state) {
              return MaterialApp(
                routes: AppRoute().getAll,
                debugShowCheckedModeBanner: false,
                theme: appTheme,
                darkTheme: appDarkTheme,
                home: const HomePage(),
                locale: state.selectedLanguage.value,
                supportedLocales: AppLocalizations.supportedLocales,
                localizationsDelegates: AppLocalizations.localizationsDelegates,
              );
            });
          },
        ),
      ),
    );
  }
}
